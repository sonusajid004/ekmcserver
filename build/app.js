"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var authentication_1 = require("./middlewares/authentication");
var authentication_2 = require("./routes/authentication");
require('dotenv').config({ path: __dirname + '/../.env' });
var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var cors = require("cors");
// const database = require("./models/initializeDB")
var app = express();
var router = express.Router();
var port = 8081;
process.once('SIGUSR2', function () {
    process.kill(process.pid, 'SIGUSR2');
});
process.on('SIGINT', function () {
    // this is only called on ctrl+c, not restart
    process.kill(process.pid, 'SIGINT');
});
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/", router);
router.get('/hello', authentication_1.authenticate, function (req, res) {
    res.send('The sedulous hyena ate the antelope!');
});
router.post('/register', authentication_2.createEmp);
router.post('/login', authentication_2.login);
router.post('/refresh', authentication_2.refresh);
// router.get("authenticate/login")
router.get("**", function (req, res) {
    res.send('Route not found');
});
app.listen(port);
console.log("Server started listening");
console.log(port);

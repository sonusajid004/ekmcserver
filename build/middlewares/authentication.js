"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authenticate = void 0;
var jwt = require('json-web-token');
var authenticate = function (req, res, next) {
    var accessToken = req.cookies.jwt;
    //if there is no token stored in cookies, the request is unauthorized
    if (!accessToken) {
        return res.status(403).send();
    }
    var payload;
    try {
        //use the jwt.verify method to verify the access token
        //throws an error if the token has expired or has a invalid signature
        payload = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
        next();
    }
    catch (err) {
        //if an error occured return request unauthorized error
        return res.status(401).send();
    }
    console.log("Authenticated");
    next();
};
exports.authenticate = authenticate;

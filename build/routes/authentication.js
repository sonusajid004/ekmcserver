"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.refresh = exports.login = exports.createEmp = exports.employees = exports.createUser = void 0;
var createUser = function (req, res, next) {
    var user = req.body;
    var session = global['session'];
    return session.sql("INSERT INTO EKMC.USERS (id,firstName,lastName,role) VALUES (" + user.id + ",\"" + user.firstName + "\",\"" + user.lastName + "\",\"" + user.role + "\")")
        .execute()
        .then(function () {
        return res.send("Data inserted");
    })
        .catch(function (err) {
        console.log(err);
        return res.send("Error in inserting data");
    });
};
exports.createUser = createUser;
exports.employees = [];
var createEmp = function (req, res, next) {
    var user = req.body;
    exports.employees.push(user);
    res.send('got the details');
};
exports.createEmp = createEmp;
var jwt = require('json-web-token');
// Never do this!
var login = function (req, res) {
    var username = req.body.username;
    var password = req.body.password;
    // Neither do this!
    if (!username || !password || exports.employees[username] !== password) {
        return res.status(401).json({
            error: true,
            data: 'Wrong Password.'
        });
    }
    var payload = { username: username };
    //create the access token with the shorter lifespan
    var accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
        algorithm: "HS256",
        expiresIn: process.env.ACCESS_TOKEN_LIFE
    });
    //create the refresh token with the longer lifespan
    var refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
        algorithm: "HS256",
        expiresIn: process.env.REFRESH_TOKEN_LIFE
    });
    //store the refresh token in the user array
    exports.employees[username].refreshToken = refreshToken;
    //send the access token to the client inside a cookie
    res.cookie("jwt", accessToken, { secure: true, httpOnly: true });
    res.send();
};
exports.login = login;
var refresh = function (req, res) {
    var accessToken = req.cookies.jwt;
    if (!accessToken) {
        return res.status(403).send();
    }
    var payload;
    try {
        payload = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
    }
    catch (e) {
        return res.status(401).send();
    }
    //retrieve the refresh token from the users array
    var refreshToken = exports.employees[payload.username].refreshToken;
    //verify the refresh token
    try {
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);
    }
    catch (e) {
        return res.status(401).send();
    }
    var newToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
        algorithm: "HS256",
        expiresIn: process.env.ACCESS_TOKEN_LIFE
    });
    res.cookie("jwt", newToken, { secure: true, httpOnly: true });
    res.send();
};
exports.refresh = refresh;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmpUser = void 0;
var Role;
(function (Role) {
    Role["Commisioner"] = "Commisioner";
    Role["SE"] = "SE";
    Role["EE"] = "EE";
    Role["AE"] = "AE";
})(Role || (Role = {}));
var EmpUser = /** @class */ (function () {
    function EmpUser() {
    }
    return EmpUser;
}());
exports.EmpUser = EmpUser;

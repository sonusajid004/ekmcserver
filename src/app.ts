import { authenticate } from "./middlewares/authentication";
import { createEmp,login, refresh } from "./routes/authentication";


require('dotenv').config({path: __dirname + '/../.env'})
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require ('cookie-parser')
const cors = require("cors");
// const database = require("./models/initializeDB")
const app = express();
const router = express.Router();
const port = 8081;
process.once('SIGUSR2', function () {
  process.kill(process.pid, 'SIGUSR2');
});

process.on('SIGINT', function () {
  // this is only called on ctrl+c, not restart
  process.kill(process.pid, 'SIGINT');
});

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use("/",router);

router.get('/hello',authenticate, (req, res) => {
  res.send('The sedulous hyena ate the antelope!');
});
router.post('/register',createEmp);
router.post('/login',login);
router.post('/refresh',refresh);
// router.get("authenticate/login")
router.get("**",(req,res)=>{
  res.send('Route not found');
})

app.listen(port);
console.log("Server started listening");
console.log(port);

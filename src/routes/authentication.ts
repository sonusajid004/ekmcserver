import { Http2ServerRequest, Http2ServerResponse } from "http2"
import { EmpUser, User } from "../types/types"

export const createUser = (req,res,next)=>{

    let user = req.body as User;
    const session = global['session'];
    return session.sql(`INSERT INTO EKMC.USERS (id,firstName,lastName,role) VALUES (${user.id},"${user.firstName}","${user.lastName}","${user.role}")`)
        .execute()
        .then(()=>{
            return res.send("Data inserted")
        })
        .catch((err)=>{
            console.log(err)
            return res.send("Error in inserting data");
        })


}
export const employees: EmpUser[]=[];
export const createEmp = (req,res,next)=>{
    
    let user = req.body as EmpUser;
    employees.push(user);   
    res.send('got the details');
}
const jwt = require('json-web-token')

// Never do this!

export const login = function(req, res){

    let username = req.body.username
    let password = req.body.password
    
    // Neither do this!
    if (!username || !password || employees[username] !== password){
        return res.status(401).json({
            error:true,
            data:'Wrong Password.'
        })

    }
    
    let payload = {username: username}

    //create the access token with the shorter lifespan
    let accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
        algorithm: "HS256",
        expiresIn: process.env.ACCESS_TOKEN_LIFE
    })

    //create the refresh token with the longer lifespan
    let refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
        algorithm: "HS256",
        expiresIn: process.env.REFRESH_TOKEN_LIFE
    })

    //store the refresh token in the user array
    employees[username].refreshToken = refreshToken

    //send the access token to the client inside a cookie
    res.cookie("jwt", accessToken, {secure: true, httpOnly: true})
    res.send()
} 

export const refresh = function (req, res){

    let accessToken = req.cookies.jwt

    if (!accessToken){
        return res.status(403).send()
    }

    let payload
    try{
        payload = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET)
     }
    catch(e){
        return res.status(401).send()
    }

    //retrieve the refresh token from the users array
    let refreshToken = employees[payload.username].refreshToken

    //verify the refresh token
    try{
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET)
    }
    catch(e){
        return res.status(401).send()
    }

    let newToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, 
    {
        algorithm: "HS256",
        expiresIn: process.env.ACCESS_TOKEN_LIFE
    })

    res.cookie("jwt", newToken, {secure: true, httpOnly: true})
    res.send()
}
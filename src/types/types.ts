
enum Role{
    Commisioner="Commisioner",
    SE="SE",
    EE="EE",
    AE="AE"
}

export interface User{
    id:number;
    firstName:string;
    lastName:string;
    role:Role
}

export class EmpUser{
    userName:string;
    password:string;
    emailId:string;
    refreshToken?:string;
}